/*
 * Austin Rafter
 * 013433901
 * 9/27/2020
 *
 * take the user input from constructor
 * return the volume of the can or surface area of the can
 */

public class SodaCan {
    public double dCanRadius;
    public double dCanHeight;
    double dPiAmount = Math.PI;

    public SodaCan(double mRadiusOfCan, double mHeightOfCan) {
        dCanRadius = (mRadiusOfCan / 2);
        dCanHeight = mHeightOfCan;
    }

    /*
    Return volume of can Pi * radius^2 * height
     */
    public double getVolume(){

        double dVolumeTotal = (Math.pow(dCanRadius, 2.0) * dPiAmount * dCanHeight);

        return dVolumeTotal;


    }

    /*
    return surface area of can (2 * pi * radius^2) + (2 * pi * radius * height)
     */
    public double getSurfaceArea(){

        double dSurfaceAreaTopBottom = (2 * dPiAmount * (Math.pow(dCanRadius, 2.0)));
        double dSurfaceAreaSides = (2 * dPiAmount * dCanRadius * dCanHeight);


        double dSurfaceAreaTotal = dSurfaceAreaTopBottom + dSurfaceAreaSides;

        return dSurfaceAreaTotal;

    }

}
