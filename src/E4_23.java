import java.text.DecimalFormat;
import java.util.Scanner;

/*
 * Austin Rafter
 * 013433901
 * 9/27/2020
 *
 * Take user input for height and diameter
 * as numbers separated by a space
 * print volume and surface area of a can
 */

public class E4_23 {

    public static void main(String[] args){
        System.out.println("Please enter the height and diameter separated by a space");
        Scanner scanObject = new Scanner(System.in);

        String strRadiusAndHeight = scanObject.nextLine();

        String[] splitStringToDoubles = strRadiusAndHeight.split(" ");

        DecimalFormat decimalFormattedOutput = new DecimalFormat("#.##");

        double dHeight = Double.parseDouble(splitStringToDoubles[0]);

        double dRadius = Double.parseDouble(splitStringToDoubles[1]);

        SodaCan sodaCanObjectOne = new SodaCan(dRadius, dHeight);

        System.out.println(decimalFormattedOutput.format(sodaCanObjectOne.getVolume()));

        System.out.println(decimalFormattedOutput.format(sodaCanObjectOne.getSurfaceArea()));
    }

}
